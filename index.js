var data = [
    { id: 1, name: "Lorem Ipsum", amount: 42, date: "2023-01-22", note: "Hey, this is a note :)" },
    { id: 2, name: "Lorem psum", amount: 8, date: "2023-01-22" },
    { id: 3, name: "Lorem sum", amount: 300, date: "2023-01-22", note: "Annoying voice" },
    { id: 4, name: "Lorem um", amount: 150, date: "2023-01-22", },
    { id: 5, name: "Lorem m", amount: 22, date: "2023-01-22", note: "Lorem" },
];
var conditionList = [
    function (d) { return (d.amount <= 42); },
    function (d) { return (!d.note); }
];
var result = conditionList.reduce(function (acc, cv) { return (acc.filter(function (d) { return cv(d); })); }, data);
console.log({ result: result });

# Epptec Assignment

V tomto repozitáři se nachází vše k zadanému úkolu.

Datový model se nachází v souboru "epptec ER diagram.svg" v kořenovém adresáři tohoto repozitáře. (lze zobrazit v přímo Gitlabu)

SQL dotazy se nachází v souboru "queries.sql".

Algoritmus se nachází v souborech index.ts a index.js. Kód lze vidět opět přímo v Gitlabu. Pro spuštění je třeba naklonovat (stáhnout) repozitář a poté otevřít soubor index.js v libovolném prohlížeči. (např. Chrome) V takovém případě se pak výsledek zobrazí v konzoli v nástrojích pro vývojáře (F12 -> Konzole)

Alternativně se dá skript spustit v příkazové řádce pomocí nástroje Node.js, který je ovšem třeba nejprve stáhnout a nainstalovat.

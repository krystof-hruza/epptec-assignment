/* Ukol č. 5 sekce Datový model*/
SELECT c.id, c.first_name, c.last_name
FROM client AS c
INNER JOIN account AS a ON a.id_client = c.id
INNER JOIN balance AS b ON b.id_account = a.id 
WHERE MONTH(DATE(NOW())) = MONTH(b.date)
AND SUM(b.principal) > 10000 /* arbitrární číslo */
GROUP BY c.id

/* Ukol č. 6 sekce Datový model*/
SELECT c.id, c.first_name, c.last_name, debt.total
FROM client AS c
INNER JOIN (
  SELECT a.id_client, (SUM(b.principal)+SUM(b.interest)+SUM(b.fees)) AS total
  FROM client as c
  INNER JOIN account AS a ON a.id_client = c.id
  INNER JOIN balance AS b ON b.id_account = a.id 
  WHERE MONTH(DATE(NOW())) = MONTH(b.date)
  GROUP BY c.id
) as debt ON debt.id_client = c.id
GROUP BY c.id
ORDER BY debt.total DESC
LIMIT 10
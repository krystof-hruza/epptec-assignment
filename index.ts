interface DatasetRowType {
  id: number
  name: string
  amount: number
  date: string // YYYY-MM-DD
  note?: string
}
type ConditionType = ((d: DatasetRowType) => boolean)

const data: DatasetRowType[] = [
  { id: 1, name: "Lorem Ipsum", amount: 42, date: "2023-01-22", note: "Hey, this is a note :)" },
  { id: 2, name: "Lorem psum", amount: 8, date: "2023-01-22"},
  { id: 3, name: "Lorem sum", amount: 300, date: "2023-01-22", note: "Annoying voice" },
  { id: 4, name: "Lorem um", amount: 150, date: "2023-01-22", },
  { id: 5, name: "Lorem m", amount: 22, date: "2023-01-22", note: "Lorem" },
]

const conditionList: ConditionType[] = [
  (d) => (d.amount <= 42),
  (d) => (!d.note)
]

const result = conditionList.reduce(
  (acc: DatasetRowType[], cv: ConditionType) => (
    acc.filter(d => cv(d))
  ), data
)

console.log({result})